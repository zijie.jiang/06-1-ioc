package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {

    private final Dependent dependent;
    private final String string;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
        this.string = null;
    }

    public MultipleConstructor(String string) {
        this.dependent = null;
        this.string = string;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public String getString() {
        return string;
    }
}

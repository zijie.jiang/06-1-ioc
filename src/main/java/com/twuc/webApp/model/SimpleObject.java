package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SimpleObject implements SimpleInterface {

    private final SimpleDependent simpleDependent;

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }

    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }
}

package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {

    private int order;
    private Dependent dependent;

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
        this.order = 1;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public int getOrder() {
        return order;
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent) {
        this.order = 2;
    }
}

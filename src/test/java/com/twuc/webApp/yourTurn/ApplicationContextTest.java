package com.twuc.webApp.yourTurn;

import com.twuc.outOfScan.OutOfScanningScope;
import com.twuc.webApp.model.Dependent;
import com.twuc.webApp.model.ImplementationA;
import com.twuc.webApp.model.Interface;
import com.twuc.webApp.model.InterfaceImpl;
import com.twuc.webApp.model.InterfaceWithMultipleImpls;
import com.twuc.webApp.model.MultipleConstructor;
import com.twuc.webApp.model.Pig;
import com.twuc.webApp.model.SimpleInterface;
import com.twuc.webApp.model.SimpleObject;
import com.twuc.webApp.model.WithAutowiredMethod;
import com.twuc.webApp.model.WithDependency;
import com.twuc.webApp.model.WithoutDependency;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ApplicationContextTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void createApplicationContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_successfully_create_object_with_out_dependency() {
        WithoutDependency withoutDependency = context.getBean(WithoutDependency.class);
        assertNotNull(withoutDependency);
        assertSame(WithoutDependency.class, withoutDependency.getClass());
    }

    @Test
    void should_successfully_create_object_with_dependency() {
        WithDependency withDependency = context.getBean(WithDependency.class);
        assertNotNull(withDependency);
        assertNotNull(withDependency.getDependent());
        assertSame(WithDependency.class, withDependency.getClass());
        assertSame(Dependent.class, withDependency.getDependent().getClass());
    }

    @Test
    void should_throw_exception_when_object_out_of_scan_scope() {
        assertThrows(Exception.class, () -> context.getBean(OutOfScanningScope.class));
    }

    @Test
    void should_create_successfully_given_interface() {
        Interface anInterface = context.getBean(Interface.class);
        assertNotNull(anInterface);
        assertSame(InterfaceImpl.class, anInterface.getClass());
    }

    @Test
    void should_create_bean_successfully_with_string_param() {
        SimpleInterface simpleInterface = context.getBean(SimpleInterface.class);
        assertSame(SimpleObject.class, simpleInterface.getClass());
        String name = ((SimpleObject) simpleInterface).getSimpleDependent().getName();
        assertEquals("O_o", name);
    }

    @Test
    void should_specify_constructor_when_create_bean() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean.getDependent());
        assertSame(Dependent.class, bean.getDependent().getClass());
    }

    @Test
    void should_invoke_constructor_and_invoke_initialize_method() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        assertEquals(2, bean.getOrder());
        assertNotNull(bean.getDependent());
        assertSame(Dependent.class, bean.getDependent().getClass());
    }

    @Test
    void should_return_multiple_implements_when_invoke_method_once() {
        Map<String, InterfaceWithMultipleImpls> beansOfType = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Optional<InterfaceWithMultipleImpls> first = beansOfType.values().stream()
                .filter(impl -> impl.getClass() == ImplementationA.class)
                .findFirst();
        assertTrue(first.isPresent());
    }

    @Test
    void should_return_object_with_default_value_when_use_value_annotation() {
        Pig pig = context.getBean(Pig.class);
        assertEquals(new Integer(1), pig.getWeight());
        pig.setWeight(10);
        Pig bean = context.getBean(Pig.class);
        assertEquals(new Integer(10), bean.getWeight());
    }
}
